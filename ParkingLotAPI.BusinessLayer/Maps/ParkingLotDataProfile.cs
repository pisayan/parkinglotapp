﻿using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;
using ParkingLotAPI.BusinessLayer.Models;

namespace ParkingLotAPI.BusinessLayer.Maps
{
    public class ParkingLotDataProfile : Profile
    {
        public ParkingLotDataProfile()
        {
            CreateMap<UserRequest, User>();
            CreateMap<ParkingLotRequest, ParkingLot>();
            CreateMap<SpotRequest, Spot>();
        }
    }
}
