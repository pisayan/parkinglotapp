﻿namespace ParkingLotAPI.BusinessLayer.Models
{
    public class UserRequest
    {
        public int? UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
    }
}