﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ParkingLotAPI.BusinessLayer.Models
{
    public class SpotRequest
    {
        public int? SpotId { get; set; }
        public int SpotNumber { get; set; }
        public int ParkingLotId { get; set; }
        public bool Handicap { get; set; }
        public double? ParkingRatePerHour { get; set; }
    }
}
