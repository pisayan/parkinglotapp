﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ParkingLotAPI.BusinessLayer.Models
{
    public class UserParkEventHistoryItem
    {
        public User User { get; set; }
        public Spot Spot { get; set; }
        public ParkingLot ParkingLot { get; set; }
        public DateTime? ParkingEnteredTime { get; set; }
        public DateTime? ParkingExitedTime { get; set; }
        public bool CurrentlyParked { get; set; }
    }
}
