﻿namespace ParkingLotAPI.BusinessLayer.Models
{
    public class ParkRequest
    {
        public int UserId { get; set; }
        public int SpotId { get; set; }
    }
}