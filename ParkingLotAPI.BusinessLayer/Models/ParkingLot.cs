﻿using System.Collections.Generic;

namespace ParkingLotAPI.BusinessLayer.Models
{
    public class ParkingLot
    {
        public int ParkingLotId { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public int Zip { get; set; }
        public int LotCapacity { get; set; }
        public IEnumerable<Spot> Spots { get; set; }
    }
}