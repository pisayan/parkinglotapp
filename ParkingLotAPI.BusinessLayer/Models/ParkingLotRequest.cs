﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ParkingLotAPI.BusinessLayer.Models
{
    public class ParkingLotRequest
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public int Zip { get; set; }
        public int LotCapacity { get; set; }
        public int HandicapCapacity { get; set; }
        public double ParkingRatePerHour { get; set; }
    }
}
