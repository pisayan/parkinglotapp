﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using ParkingLotAPI.BusinessLayer.Models;

namespace ParkingLotAPI.BusinessLayer.Services
{
    public class ParkingLotBusinessService : IParkingLotBusinessService
    {
        private IParkingLotDataAccessService _dataService;
        private IMapper _mapper;

        public ParkingLotBusinessService(IParkingLotDataAccessService dataService, IMapper mapper)
        {
            _dataService = dataService;
            _mapper = mapper;
        }

        public async Task<IEnumerable<ParkingLot>> GetParkingLots()
        {
            return await _dataService.GetParkingLots();
        }

        public async Task<IEnumerable<ParkingLot>> GetParkingLots(int zip)
        {
            return await _dataService.GetParkingLots(zip);
        }

        public async Task<ParkingLot> GetParkingLot(int id)
        {
            return await _dataService.GetParkingLot(id);
        }

        public async Task<ParkingLot> AddParkingLot(ParkingLotRequest request)
        {
            // Create Lot Entity
            ParkingLot lot = await _dataService.AddParkingLot(request);

            int handicapQty = request.HandicapCapacity;

            // Add Lot Spots
            for (var i = 1; i <= request.LotCapacity; i++)
            {
                SpotRequest spotRequest = new SpotRequest
                {
                    ParkingLotId = lot.ParkingLotId,
                    ParkingRatePerHour = request.ParkingRatePerHour,
                    SpotNumber = i,
                    Handicap = false
                };

                if (handicapQty > 0)
                {
                    spotRequest.Handicap = true;
                    handicapQty--;
                }

                await _dataService.AddSpot(spotRequest);
            }

            return lot;
        }

        public async Task<ParkingLot> EditParkingLot(int id, ParkingLotRequest request)
        {

            ParkingLot lot = await _dataService.EditParkingLot(id, request);
            return lot;
        }

        public async Task<IEnumerable<User>> GetUsers()
        {
            return await _dataService.GetUsers();
        }

        public async Task<User> GetUser(int id)
        {
            return await _dataService.GetUser(id);
        }

        public async Task<User> GetUser(string email)
        {
            return await _dataService.GetUser(email);
        }

        public async Task<User> AddUser(UserRequest request)
        {
            int newUserId = await _dataService.AddUser(request);
            return await _dataService.GetUser(newUserId);
        }

        public async Task<User> EditUser(UserRequest request)
        {
            int userId = await _dataService.EditUser(request);
            return await _dataService.GetUser(userId);
        }

        public async Task<UserParkingEvent> ParkUser(ParkRequest request)
        {
            UserParkingEvent parkRequest = new UserParkingEvent();
            parkRequest.Spot = await _dataService.GetSpot(request.SpotId);
            parkRequest.User = await _dataService.GetUser(request.UserId);
            parkRequest.CurrentlyParked = true;
            var currentSpot = await _dataService.GetUsersCurrentSpot(request.UserId);
            if ( currentSpot == null)
            {
                return await _dataService.ParkUser(parkRequest);
            }
            else
            {
                return null;
            }
        }

        public async Task<UserParkingEvent> DepartUser(ParkRequest request)
        {
            UserParkingEvent parkRequest = new UserParkingEvent();
            parkRequest.Spot = await _dataService.GetSpot(request.SpotId);
            parkRequest.User = await _dataService.GetUser(request.UserId);
            parkRequest.CurrentlyParked = false;

            return await _dataService.DepartUser(parkRequest);
        }

        public Task<IEnumerable<UserParkingEvent>> GetLotsCurrentUsers(int id)
        {
            return _dataService.GetLotsCurrentUsers(id);
        }

        public Task<IEnumerable<UserParkingEvent>> GetLotsUserHistory(int id)
        {
            return _dataService.GetLotsUserHistory(id);
        }

        public Task<ParkingLot> GetUsersCurrentLot(int id)
        {
            return _dataService.GetUsersCurrentLot(id);
        }

        public Task<Spot> GetUsersCurrentSpot(int id)
        {
            return _dataService.GetUsersCurrentSpot(id);
        }

        public Task<IEnumerable<UserParkEventHistoryItem>> GetUsersParkingHistory(int id)
        {
            return _dataService.GetUsersParkingHistory(id);
        }

        public Task<Spot> GetSpot(int id)
        {
            return _dataService.GetSpot(id);
        }

        public Task<IEnumerable<Spot>> GetParkingLotsSpots(int id)
        {
            return _dataService.GetParkingLotsSpots(id);
        }

        public Task<Spot> GetFirstAvailableSpotInLot(int id, bool handicap = false)
        {
            return _dataService.GetFirstAvailableSpotInLot(id, handicap);
        }

        public Task<Spot> AddSpot(SpotRequest request)
        {
            return _dataService.AddSpot(request);
        }

        public Task<Spot> EditSpot(SpotRequest request)
        {
            return _dataService.EditSpot(request);
        }
    }
}
