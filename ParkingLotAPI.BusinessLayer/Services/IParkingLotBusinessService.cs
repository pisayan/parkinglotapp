﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using ParkingLotAPI.BusinessLayer.Models;

namespace ParkingLotAPI.BusinessLayer.Services
{
    public interface IParkingLotBusinessService
    {
        Task<IEnumerable<ParkingLot>> GetParkingLots();
        Task<IEnumerable<ParkingLot>> GetParkingLots(int zip);
        Task<ParkingLot> GetParkingLot(int id);
        Task<ParkingLot> AddParkingLot(ParkingLotRequest request);
        Task<ParkingLot> EditParkingLot(int id, ParkingLotRequest request);

        Task<IEnumerable<User>> GetUsers();
        Task<User> GetUser(int id);
        Task<User> GetUser(string email);
        Task<User> AddUser(UserRequest request);
        Task<User> EditUser(UserRequest request);

        Task<UserParkingEvent> ParkUser(ParkRequest request);
        Task<UserParkingEvent> DepartUser(ParkRequest request);
        Task<IEnumerable<UserParkingEvent>> GetLotsCurrentUsers(int id);
        Task<IEnumerable<UserParkingEvent>> GetLotsUserHistory(int id);
        Task<ParkingLot> GetUsersCurrentLot(int id);
        Task<Spot> GetUsersCurrentSpot(int id);
        Task<IEnumerable<UserParkEventHistoryItem>> GetUsersParkingHistory(int id);

        Task<Spot> GetSpot(int id);
        Task<IEnumerable<Spot>> GetParkingLotsSpots(int id);
        Task<Spot> GetFirstAvailableSpotInLot(int id, bool handicap = false);
        Task<Spot> AddSpot(SpotRequest request);
        Task<Spot> EditSpot(SpotRequest request);
    }
}
