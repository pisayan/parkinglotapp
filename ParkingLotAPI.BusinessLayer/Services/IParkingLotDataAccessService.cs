﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using ParkingLotAPI.BusinessLayer.Models;

namespace ParkingLotAPI.BusinessLayer.Services
{
    public interface IParkingLotDataAccessService
    {
        Task<IEnumerable<User>> GetUsers();

        Task<User> GetUser(int id);

        Task<User> GetUser(string email);
        Task<int> AddUser(UserRequest request);
        Task<int> EditUser(UserRequest request);

        Task<IEnumerable<ParkingLot>> GetParkingLots();

        Task<ParkingLot> GetParkingLot(int id);

        Task<IEnumerable<ParkingLot>> GetParkingLots(int Zip);
        Task<ParkingLot> AddParkingLot(ParkingLotRequest request);
        Task<ParkingLot> EditParkingLot(int id, ParkingLotRequest request);

        Task<Spot> AddSpot(SpotRequest request);
        Task<Spot> EditSpot(SpotRequest request);
        Task<Spot> GetSpot(int id);
        Task<IEnumerable<Spot>> GetParkingLotsSpots(int id);
        Task<Spot> GetFirstAvailableSpotInLot(int id, bool handicap = false);

        Task<UserParkingEvent> ParkUser(UserParkingEvent request);
        Task<UserParkingEvent> DepartUser(UserParkingEvent request);
        Task<IEnumerable<UserParkingEvent>> GetLotsCurrentUsers(int id);
        Task<IEnumerable<UserParkingEvent>> GetLotsUserHistory(int id);
        Task<Spot> GetUsersCurrentSpot(int id);
        Task<ParkingLot> GetUsersCurrentLot(int id);
        Task<IEnumerable<UserParkEventHistoryItem>> GetUsersParkingHistory(int id);
    }
}
