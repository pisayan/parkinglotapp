﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ParkingLotAPI.DataAccess.Migrations
{
    public partial class ChangePKforUserParkingEvents : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_UserParkingEvents",
                table: "UserParkingEvents");

            migrationBuilder.AddColumn<int>(
                name: "EventId",
                table: "UserParkingEvents",
                nullable: false,
                defaultValue: 0)
                .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            migrationBuilder.AddPrimaryKey(
                name: "PK_UserParkingEvents",
                table: "UserParkingEvents",
                column: "EventId");

            migrationBuilder.CreateIndex(
                name: "IX_UserParkingEvents_UserId",
                table: "UserParkingEvents",
                column: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_UserParkingEvents",
                table: "UserParkingEvents");

            migrationBuilder.DropIndex(
                name: "IX_UserParkingEvents_UserId",
                table: "UserParkingEvents");

            migrationBuilder.DropColumn(
                name: "EventId",
                table: "UserParkingEvents");

            migrationBuilder.AddPrimaryKey(
                name: "PK_UserParkingEvents",
                table: "UserParkingEvents",
                columns: new[] { "UserId", "SpotId" });
        }
    }
}
