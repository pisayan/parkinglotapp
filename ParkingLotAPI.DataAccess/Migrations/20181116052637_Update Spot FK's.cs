﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ParkingLotAPI.DataAccess.Migrations
{
    public partial class UpdateSpotFKs : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Spots_ParkingLots_ParkingLotId",
                table: "Spots");

            migrationBuilder.AlterColumn<int>(
                name: "ParkingLotId",
                table: "Spots",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Spots_ParkingLots_ParkingLotId",
                table: "Spots",
                column: "ParkingLotId",
                principalTable: "ParkingLots",
                principalColumn: "ParkingLotId",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Spots_ParkingLots_ParkingLotId",
                table: "Spots");

            migrationBuilder.AlterColumn<int>(
                name: "ParkingLotId",
                table: "Spots",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddForeignKey(
                name: "FK_Spots_ParkingLots_ParkingLotId",
                table: "Spots",
                column: "ParkingLotId",
                principalTable: "ParkingLots",
                principalColumn: "ParkingLotId",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
