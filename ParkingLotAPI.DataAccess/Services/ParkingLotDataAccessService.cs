﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using ParkingLotAPI.BusinessLayer.Services;
using ParkingDAL = ParkingLotAPI.DataAccess.Models;
using ParkingBLL = ParkingLotAPI.BusinessLayer.Models;

namespace ParkingLotAPI.DataAccess.Services
{
    public class ParkingLotDataAccessService : IParkingLotDataAccessService
    {
        private IMapper _mapper;
        private ParkingDAL.ParkingLotContext _context;
        public ParkingLotDataAccessService(ParkingDAL.ParkingLotContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        #region User Methods

        public async Task<IEnumerable<ParkingBLL.User>> GetUsers()
        {
            var users =  _context.Users.ToList().AsEnumerable();
            return _mapper.Map<IEnumerable<ParkingDAL.User>, IEnumerable<ParkingBLL.User>>(users);
        }

        public async Task<ParkingBLL.User> GetUser(int id)
        {
            ParkingDAL.User user =  _context.Users.FirstOrDefault(s => s.UserId == id);
            return _mapper.Map<ParkingBLL.User>(user);
        }

        public async Task<ParkingBLL.User> GetUser(string email)
        {
            ParkingDAL.User user = _context.Users.FirstOrDefault(s => s.Email == email);
            return _mapper.Map<ParkingBLL.User>(user);
        }

        public async Task<int> AddUser(ParkingBLL.UserRequest request)
        {
            ParkingDAL.User user = _mapper.Map<ParkingDAL.User>(request);
            _context.Users.Add(user);
            await _context.SaveChangesAsync();
            return user.UserId;
        }

        public async Task<int> EditUser(ParkingBLL.UserRequest request)
        {
            ParkingDAL.User user = _context.Users.FirstOrDefault(s => s.UserId == request.UserId);

            _context.Users.Update(EditUserObject(user,request));
            await _context.SaveChangesAsync();
            return user.UserId;
        }
        public async Task<ParkingBLL.Spot> GetUsersCurrentSpot(int id)
        {
            ParkingDAL.Spot spot = _context.Spots.FirstOrDefault(s => s.UserId == id);
            return _mapper.Map<ParkingDAL.Spot, ParkingBLL.Spot>(spot);
        }

        public async Task<ParkingBLL.ParkingLot> GetUsersCurrentLot(int id)
        {
            ParkingDAL.ParkingLot lot = _context.ParkingLots.FirstOrDefault(s => s.Spots.Any(a => a.UserId == id));

            return _mapper.Map<ParkingDAL.ParkingLot, ParkingBLL.ParkingLot>(lot);
        }

        public async Task<IEnumerable<ParkingBLL.UserParkEventHistoryItem>> GetUsersParkingHistory(int id)
        {
            List<ParkingDAL.UserParkingEvent> events = await _context.UserParkingEvents.Where(s => s.UserId == id).Include(s => s.Spot).Include(s => s.User).ToListAsync();

            List<ParkingBLL.UserParkEventHistoryItem> eventItems = new List<ParkingBLL.UserParkEventHistoryItem>();
            foreach (var evnt in events)
            {
                var item = new ParkingBLL.UserParkEventHistoryItem();
                ParkingDAL.ParkingLot lot = _context.ParkingLots.FirstOrDefault(s => s.ParkingLotId == evnt.Spot.ParkingLotId);
                item.User = _mapper.Map<ParkingBLL.User>(evnt.User);
                item.Spot = _mapper.Map<ParkingBLL.Spot>(evnt.Spot);
                item.ParkingLot = _mapper.Map<ParkingBLL.ParkingLot>(lot);
                item.CurrentlyParked = evnt.CurrentlyParked;
                item.ParkingEnteredTime = evnt.ParkingEnteredTime;
                item.ParkingExitedTime = evnt.ParkingExitedTime;
                eventItems.Add(item);
            }

            return eventItems;
        }
        #endregion

        #region Parking Lot Methods

        public async Task<IEnumerable<ParkingBLL.ParkingLot>> GetParkingLots()
        {
            IEnumerable<ParkingDAL.ParkingLot> lots = _context.ParkingLots.ToList();
            return _mapper.Map<IEnumerable<ParkingBLL.ParkingLot>>(lots);
        }

        public async Task<ParkingBLL.ParkingLot> GetParkingLot(int id)
        {
            ParkingDAL.ParkingLot lot = _context.ParkingLots.FirstOrDefault(s => s.ParkingLotId == id);
            return _mapper.Map<ParkingBLL.ParkingLot>(lot);
        }

        public async Task<IEnumerable<ParkingBLL.ParkingLot>> GetParkingLots(int Zip)
        {
            var lots = _context.ParkingLots.Where(s => s.Zip == Zip).ToListAsync();
            return _mapper.Map<IEnumerable<ParkingBLL.ParkingLot>>(lots);
        }

        public async Task<ParkingBLL.ParkingLot> AddParkingLot(ParkingBLL.ParkingLotRequest request)
        {
            ParkingDAL.ParkingLot lot = _mapper.Map<ParkingDAL.ParkingLot>(request);
            _context.ParkingLots.Add(lot);
            await _context.SaveChangesAsync();
            return _mapper.Map<ParkingDAL.ParkingLot, ParkingBLL.ParkingLot>(lot);
        }

        public async Task<ParkingBLL.ParkingLot> EditParkingLot(int id, ParkingBLL.ParkingLotRequest request)
        {
            ParkingDAL.ParkingLot lot = _context.ParkingLots.FirstOrDefault(s => s.ParkingLotId == id);
            _context.ParkingLots.Update(EditParkingLotObject(lot, request));

            await _context.SaveChangesAsync();
            return _mapper.Map<ParkingDAL.ParkingLot, ParkingBLL.ParkingLot>(lot);
        }

        public async Task<IEnumerable<ParkingBLL.Spot>> GetParkingLotsSpots(int id)
        {
            IEnumerable<ParkingDAL.Spot> spots =
                _context.Spots.Where(s => s.ParkingLotId == id)
                    .Include(s => s.User)
                    .AsEnumerable();

            return _mapper.Map<IEnumerable<ParkingDAL.Spot>, IEnumerable<ParkingBLL.Spot>>(spots);
        }

        public async Task<ParkingBLL.Spot> GetFirstAvailableSpotInLot(int id, bool handicap = false)
        {
            ParkingDAL.Spot spot = _context.Spots.OrderBy(s => s.SpotNumber)
                .Where(s => s.ParkingLotId == id && s.Handicap == handicap && s.UserId == null)
                .Include(s => s.User)
                .FirstOrDefault();

            return _mapper.Map<ParkingDAL.Spot, ParkingBLL.Spot>(spot);
        }


        public async Task<IEnumerable<ParkingBLL.UserParkingEvent>> GetLotsCurrentUsers(int id)
        {
            IEnumerable<ParkingDAL.UserParkingEvent> users =
                _context.UserParkingEvents.Where(s => s.CurrentlyParked == true && s.Spot.ParkingLotId == id)
                    .Include(userPark => userPark.User)
                    .Include(userPark => userPark.Spot)
                    .AsEnumerable();
            return _mapper.Map<IEnumerable<ParkingDAL.UserParkingEvent>, IEnumerable<ParkingBLL.UserParkingEvent>>(users);
        }

        public async Task<IEnumerable<ParkingBLL.UserParkingEvent>> GetLotsUserHistory(int id)
        {
            IEnumerable<ParkingDAL.UserParkingEvent> users =
                _context.UserParkingEvents.Where(s => s.Spot.ParkingLotId == id)
                    .Include(userPark => userPark.User)
                    .Include(userPark => userPark.Spot)
                    .AsEnumerable();
            return _mapper.Map<IEnumerable<ParkingDAL.UserParkingEvent>, IEnumerable<ParkingBLL.UserParkingEvent>>(users);
        }

        #endregion

        #region Spot Methods

        public async Task<ParkingBLL.Spot> GetSpot(int id)
        {
            ParkingDAL.Spot spot = _context.Spots.FirstOrDefault(s => s.SpotId == id);
            return _mapper.Map<ParkingDAL.Spot, ParkingBLL.Spot>(spot);
        }
        public async Task<ParkingBLL.Spot> AddSpot(ParkingBLL.SpotRequest request)
        {
            ParkingDAL.Spot spot = _mapper.Map<ParkingBLL.SpotRequest, ParkingDAL.Spot>(request);
            _context.Spots.Add(spot);
            await _context.SaveChangesAsync();

            return _mapper.Map<ParkingDAL.Spot, ParkingBLL.Spot>(spot);
        }

        public async Task<ParkingBLL.Spot> EditSpot(ParkingBLL.SpotRequest request)
        {
            ParkingDAL.Spot spot = _context.Spots.FirstOrDefault(s => s.SpotId == request.SpotId);
            _context.Spots.Update(EditSpotObject(spot, request));
            await _context.SaveChangesAsync();

            return _mapper.Map<ParkingDAL.Spot, ParkingBLL.Spot>(spot);
        }
        public async Task<ParkingBLL.UserParkingEvent> ParkUser(ParkingBLL.UserParkingEvent request)
        {
            ParkingDAL.UserParkingEvent userPark = new ParkingDAL.UserParkingEvent
            {
                SpotId = request.Spot.SpotId,
                UserId = request.User.UserId,
                CurrentlyParked = request.CurrentlyParked
            };

            _context.UserParkingEvents.Add(userPark);

            ParkingDAL.Spot spot = _context.Spots.FirstOrDefault(s => s.SpotId == request.Spot.SpotId);
            spot.UserId = request.User.UserId;
            _context.Spots.Update(spot);

            await _context.SaveChangesAsync();

            return _mapper.Map<ParkingDAL.UserParkingEvent, ParkingBLL.UserParkingEvent>(userPark);
        }

        public async Task<ParkingBLL.UserParkingEvent> DepartUser(ParkingBLL.UserParkingEvent request)
        {
            List<ParkingDAL.UserParkingEvent> userParks = _context.UserParkingEvents.Where(s =>
                    s.UserId == request.User.UserId && s.Spot.SpotId == request.Spot.SpotId &&
                    s.CurrentlyParked)
                .ToList();
            foreach (var userPark in userParks)
            {
                userPark.CurrentlyParked = false;
                userPark.ParkingExitedTime = DateTime.Now;
                _context.UserParkingEvents.Update(userPark);
            }

            ParkingDAL.Spot spot = _context.Spots.FirstOrDefault(s => s.SpotId == request.Spot.SpotId);
            spot.UserId = null;
            _context.Spots.Update(spot);
            await _context.SaveChangesAsync();

            ParkingDAL.UserParkingEvent updatedEvent =
                _context.UserParkingEvents.Where(
                        s => s.UserId == request.User.UserId &&
                             s.SpotId == request.Spot.SpotId)
                    .OrderByDescending(y => y.ParkingExitedTime).FirstOrDefault(y => true);

            return _mapper.Map<ParkingDAL.UserParkingEvent, ParkingBLL.UserParkingEvent>(updatedEvent);
        }

        #endregion

        #region Helper Methods
        private ParkingDAL.User EditUserObject(ParkingDAL.User user, ParkingBLL.UserRequest request)
        {
            user.Email = request.Email;
            user.FirstName = request.FirstName;
            user.LastName = request.LastName;
            return user;
        }

        private ParkingDAL.ParkingLot EditParkingLotObject(ParkingDAL.ParkingLot lot, ParkingBLL.ParkingLotRequest request)
        {
            lot.Name = request.Name;
            lot.Address = request.Address;
            lot.City = request.City;
            lot.State = request.State;
            lot.Zip = request.Zip;
            lot.LotCapacity = request.LotCapacity;
            return lot;
        }

        private ParkingDAL.Spot EditSpotObject(ParkingDAL.Spot spot, ParkingBLL.SpotRequest request)
        {
            spot.Handicap = request.Handicap;
            spot.ParkingRatePerHour = request.ParkingRatePerHour;
            return spot;
        }

        #endregion

    }
}
