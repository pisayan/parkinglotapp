﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Microsoft.EntityFrameworkCore;

namespace ParkingLotAPI.DataAccess.Models
{
    public class ParkingLot
    {
        [Key]
        public int ParkingLotId { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public int Zip { get; set; }
        public int LotCapacity { get; set; }
        
        public virtual IEnumerable<Spot> Spots { get; set; }
    }
}
