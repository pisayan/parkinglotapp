﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;

namespace ParkingLotAPI.DataAccess.Models
{

    public class ParkingLotContextFactory : IDesignTimeDbContextFactory<ParkingLotContext>
    {
        private readonly IConfiguration _config;

        public ParkingLotContext CreateDbContext(string[] args)
        {
            var Builder = new DbContextOptionsBuilder<ParkingLotContext>();
            var connection = "Data Source=parkinglotdbserver.database.windows.net;Persist Security Info=True;Initial Catalog=parkinglot;User ID=;Password=;Pooling=False;MultipleActiveResultSets=False;Connect Timeout=60;Encrypt=False;TrustServerCertificate=True";
            Builder.UseSqlServer(connection);

            return new ParkingLotContext(Builder.Options);
        }
    }
}
