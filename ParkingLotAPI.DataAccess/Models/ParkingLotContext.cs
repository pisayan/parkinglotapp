﻿using Microsoft.EntityFrameworkCore;

namespace ParkingLotAPI.DataAccess.Models
{
    public class ParkingLotContext : DbContext
    {
        public ParkingLotContext(DbContextOptions<ParkingLotContext> options)
            : base(options)
        { }

        public DbSet<ParkingLot> ParkingLots { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<UserParkingEvent> UserParkingEvents { get; set; }
        public DbSet<Spot> Spots { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<UserParkingEvent>()
                .HasKey(c => c.EventId);

            modelBuilder.Entity<UserParkingEvent>()
                .Property(p => p.CurrentlyParked)
                .HasDefaultValue(false);

            modelBuilder.Entity<UserParkingEvent>()
                .Property(p => p.ParkingEnteredTime)
                .HasDefaultValueSql("getdate()");

        }
    }
}