﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ParkingLotAPI.DataAccess.Models
{
    public class Spot
    {
        [Key] public int SpotId { get; set; }
        public int SpotNumber { get; set; }
        public int ParkingLotId { get; set; }
        public int? UserId { get; set; }
        public bool Handicap { get; set; }
        public double? ParkingRatePerHour { get; set; }

        public virtual User User { get; set; }
        public virtual ICollection<UserParkingEvent> UserParkingEvents { get; set; }
    }
}
