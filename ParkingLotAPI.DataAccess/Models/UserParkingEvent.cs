﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ParkingLotAPI.DataAccess.Models
{
    public class UserParkingEvent
    {
        public int EventId { get; set; }
        public int UserId { get; set; }
        public int SpotId { get; set; }

        public DateTime? ParkingEnteredTime { get; set; }
        public DateTime? ParkingExitedTime { get; set; }
        public bool CurrentlyParked { get; set; }

        public virtual User User { get; set; }
        public virtual Spot Spot { get; set; }
    }
}
