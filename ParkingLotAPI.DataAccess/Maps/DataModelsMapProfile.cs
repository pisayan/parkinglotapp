﻿using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;
using ParkingDAL = ParkingLotAPI.DataAccess.Models;
using ParkingBLL = ParkingLotAPI.BusinessLayer.Models;

namespace ParkingLotAPI.DataAccess.Maps
{
    public class DataModelsMapProfile : Profile
    {
        public DataModelsMapProfile()
        {
            CreateMap<ParkingDAL.User, ParkingBLL.User>();
            CreateMap<ParkingDAL.Spot, ParkingBLL.Spot>();
            CreateMap<ParkingDAL.ParkingLot, ParkingBLL.ParkingLot>();
            CreateMap<ParkingDAL.UserParkingEvent, ParkingBLL.UserParkingEvent>()
                .ForMember(dest => dest.User,
                opts => opts.MapFrom(src => src.User));

            CreateMap<ParkingBLL.UserRequest, ParkingDAL.User>();
            CreateMap<ParkingBLL.SpotRequest, ParkingDAL.Spot>();
            CreateMap<ParkingBLL.ParkingLotRequest, ParkingDAL.ParkingLot>();
            CreateMap<ParkingBLL.UserParkingEvent, ParkingDAL.UserParkingEvent>();
        }
    }
}
