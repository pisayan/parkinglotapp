﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Swashbuckle.AspNetCore.Swagger;
using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using ParkingLotAPI.Api.AppStart;
using Serilog;
using ParkingLotAPI.DataAccess.Models;
using Microsoft.EntityFrameworkCore;
using ParkingLotAPI.API.AppStart;

namespace ParkingLotAPI
{
    public class Startup
    {
        private readonly IHostingEnvironment _env;
        private readonly IConfiguration _config;

        public Startup(IConfiguration configuration, IHostingEnvironment env)
        {
            _config = configuration;
            _env = env;
            SetupLogger();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseCors(builder =>
                builder.WithOrigins("https://localhost.com")
                    .AllowAnyHeader()
                    .AllowAnyOrigin()
                    .AllowAnyMethod());

            // Optional: To use authentication, configure it in ConfigureServices
            app.UseAuthentication();
            app.UseStaticFiles();
            app.UseCookiePolicy();
            app.UseSwagger();
            app.UseSwaggerUI(c => {
                c.SwaggerEndpoint("/swagger/" + _config.GetSection("Settings").GetSection("Version").Value + "/swagger.json", 
                _config.GetSection("Settings").GetSection("AppName").Value + " " + _config.GetSection("Settings").GetSection("Version").Value);
            });
            app.UseHttpsRedirection();
            app.UseMvc();
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            services.AddCors();
            
            // Configure AutoMapper Configuration
            services.ConfigureAutoMapper(_config);

            // Configure Dependencies resides in AppStart/Dependencies.cs
            services.ConfigureDependencies();

            // Configure Authentication resides in AppStart/Authentication.cs

            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;

            }).AddJwtBearer(options =>
            {
                options.Authority = "https://pisayan.auth0.com/";
                options.Audience = "https://parkinglotserver";
            });

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc(_config.GetSection("Settings").GetSection("Version").Value, 
                    new Info() { Title = _config.GetSection("Settings").GetSection("AppName").Value, Version = _config.GetSection("Settings").GetSection("Version").Value });
                c.AddSecurityDefinition("Bearer", new ApiKeyScheme { In = "header", Description = "Please enter JWT with Bearer into field", Name = "Authorization", Type = "apiKey" });
                c.AddSecurityRequirement(new Dictionary<string, IEnumerable<string>>
                {
                    {"Bearer", Enumerable.Empty<string>()},
                });
            });

            var connection = "Data Source=parkinglotdbserver.database.windows.net;Persist Security Info=True;Initial Catalog=parkinglot;User ID=;Password=;Pooling=False;MultipleActiveResultSets=False;Connect Timeout=60;Encrypt=False;TrustServerCertificate=True";
            services.AddDbContext<ParkingLotContext>
                (options => options.UseSqlServer(connection));
        }

        private void SetupLogger()
        {
            Log.Logger = new LoggerConfiguration()
                .ReadFrom.Configuration(_config)
                .CreateLogger();
        }
    }
}
