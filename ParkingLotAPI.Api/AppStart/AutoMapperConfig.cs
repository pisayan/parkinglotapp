﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using ParkingLotAPI.API.Maps;
using ParkingLotAPI.BusinessLayer.Maps;
using ParkingLotAPI.DataAccess.Maps;

namespace ParkingLotAPI.API.AppStart
{
    public static class AutoMapperConfig
    {
        public static void ConfigureAutoMapper(this IServiceCollection services, IConfiguration configuration)
        {
            IMapper mapper = ConfigureMapper().CreateMapper();
            services.AddSingleton(mapper);
        }

        public static MapperConfiguration ConfigureMapper()
        {
            return new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<ParkingLotDataProfile>();
                cfg.AddProfile<DataModelsMapProfile>();
                cfg.AddProfile<ParkingLotApiMapProfile>();
            });
        }
    }
}
