﻿using Microsoft.Extensions.DependencyInjection;
using ParkingLotAPI.BusinessLayer.Services;
using ParkingLotAPI.DataAccess.Services;

namespace ParkingLotAPI.Api.AppStart
{
    public static class Dependencies
    {
        public static void ConfigureDependencies(this IServiceCollection services)
        {
            // Example to configure an EF context
			// var connString = configuration.GetConnectionString("EntitiesDb");
            // services.AddDbContext<EntityContext>(options => options.UseSqlServer(connString));
           
            services.AddScoped<IParkingLotBusinessService, ParkingLotBusinessService>();
            services.AddScoped<IParkingLotDataAccessService, ParkingLotDataAccessService>();
        }
    }
}
