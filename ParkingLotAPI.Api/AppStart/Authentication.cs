﻿using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authentication.OpenIdConnect;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace ParkingLotAPI.Api.AppStart
{
    public static class Authentication
    {
        public static void ConfigureAuthentication(this IServiceCollection services, IConfiguration config)
        {
            var authority = config["Authentication:Authority"];
            var audience = config["Authentication:ClientId"];

            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = CookieAuthenticationDefaults.AuthenticationScheme;
                options.DefaultSignInScheme = CookieAuthenticationDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = CookieAuthenticationDefaults.AuthenticationScheme;
            })

            // Azure AD Authentication requires cookies
            .AddCookie(options =>
            {
                // Optional: Configure response to return 401 Unauthorized if not signed in
                options.Events.OnRedirectToLogin = ctx =>
                {
                    ctx.Response.StatusCode = StatusCodes.Status401Unauthorized;
                    return ctx.Response.WriteAsync("Unauthorized");
                };
                options.Cookie.SameSite = SameSiteMode.None;
            })

            // Optional: Use AddOpenIdConnect for Azure AD Authentication
            .AddOpenIdConnect(OpenIdConnectDefaults.AuthenticationScheme, options =>
            {
                config.GetSection("Authentication").Bind(options);
            })

            // Optional: Use AddJwtBearer for token authentication
            .AddJwtBearer(JwtBearerDefaults.AuthenticationScheme, options =>
            {
                options.Authority = authority;
                options.Audience = audience;

                // To configure token validation options, see Microsoft documentation on TokenValidationParameters Class
                // https://msdn.microsoft.com/en-us/library/system.identitymodel.tokens.tokenvalidationparameters(v=vs.114).aspx
                //options.TokenValidationParameters = new Microsoft.IdentityModel.Tokens.TokenValidationParameters { };
            });
        }
    }
}
