﻿using System;
using System.IO;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.PlatformAbstractions;
using Serilog;


namespace ParkingLotAPI
{
    public class Program
    {
        private static string _environmentName;

        public static void Main(string[] args)
        {
            var configuration = CreateConfiguration(args);

            Log.Logger = new LoggerConfiguration()
                .ReadFrom.Configuration(configuration)
                .CreateLogger();

            var host = CreateWebHostBuilder(args, configuration);

            try
            {
                Log.Information($"[{nameof(Main)}] Starting web host ...");
                Log.Information(Environment.GetEnvironmentVariable("Settings"));
                host.Run();
            }
            catch (Exception ex)
            {
                Log.Fatal(ex, $"[{nameof(Main)}] web host terminated unexpectedly");
            }
            finally
            {
                Log.CloseAndFlush();
            }
        }

        public static IConfiguration CreateConfiguration(string[] args)
        {
            var environmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            string baseDir = PlatformServices.Default.Application.ApplicationBasePath;

            var configuration = new ConfigurationBuilder()
                .AddCommandLine(args)
                .AddEnvironmentVariables()
                .AddJsonFile(Path.Combine(baseDir, "appsettings.json"), optional: true, reloadOnChange: true)
                .AddJsonFile(Path.Combine(baseDir, $"appsettings.{environmentName ?? "Production"}.json"), optional: true, reloadOnChange: true)
                .Build();

            return configuration;
        }
        /*
        public static IWebHost CreateWebHostBuilder(string[] args, IConfiguration config)
        {
            IWebHostBuilder builder = WebHost.CreateDefaultBuilder(args);
            builder
                .UseConfiguration(config)
                .UseKestrel()
                .UseSerilog()
                .Build();

            return builder;
        }
        */
        public static IWebHost CreateWebHostBuilder(string[] args, IConfiguration config) =>
            WebHost.CreateDefaultBuilder(args)
                .UseConfiguration(config)
                .UseSerilog()
                .UseStartup<Startup>()
                .Build();
    }
}
