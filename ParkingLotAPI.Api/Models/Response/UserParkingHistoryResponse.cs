﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ParkingLotAPI.API.Models.Response
{
    public class UserParkingHistoryResponse
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string ParkingLotName { get; set; }
        public string ParkingLotAddress { get; set; }
        public string ParkingLotCity { get; set; }
        public string ParkingLotState { get; set; }
        public string ParkingLotZip { get; set; }
        public int SpotNumber { get; set; }
        public DateTime? ParkingEnteredTime { get; set; }
        public DateTime? ParkingExitedTime { get; set; }
    }
}
