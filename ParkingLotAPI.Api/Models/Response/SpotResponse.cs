﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ParkingLotAPI.API.Models.Response
{
    public class SpotResponse
    {
        public int SpotId { get; set; }
        public int SpotNumber { get; set; }
        public int ParkingLotId { get; set; }
        public UserResponse User { get; set; }
        public bool Handicap { get; set; }
        public double? ParkingRatePerHour { get; set; }
    }
}
