﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ParkingLotAPI.API.Models.Response
{
    public class UserParkingLotResponse
    {
        public int UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public int SpotId { get; set; }
        public DateTime? ParkingEnteredTime { get; set; }
        public DateTime? ParkingExitedTime { get; set; }
        public bool CurrentlyParked { get; set; }
    }
}
