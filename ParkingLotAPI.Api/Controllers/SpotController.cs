﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ParkingLotAPI.API.Models.Response;
using ParkingBLL = ParkingLotAPI.BusinessLayer.Models;
using ParkingLotAPI.BusinessLayer.Services;
using Serilog;
namespace ParkingLotAPI.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class SpotController : ControllerBase
    {
        private readonly IParkingLotBusinessService _parkingLotBusinessService;
        private readonly IMapper _mapper;

        public SpotController(IParkingLotBusinessService lotService, IMapper mapper)
        {
            _parkingLotBusinessService = lotService;
            _mapper = mapper;
        }

        [HttpPost]
        [Route("Add")]
        public async Task<ActionResult> AddSpot([FromBody] ParkingBLL.SpotRequest request)
        {
            try
            {
                var result = await _parkingLotBusinessService.AddSpot(request);
                return Ok(result);
            }
            catch (Exception e)
            {
                Log.Error(e, "Stack Trace Example {StackTrace}", e.StackTrace);
                return BadRequest(e.Message);
            }
        }

        [HttpPost]
        [Route("Edit")]
        public async Task<ActionResult> EditSpot([FromBody] ParkingBLL.SpotRequest request)
        {
            try
            {
                var result = await _parkingLotBusinessService.EditSpot(request);
                return Ok(result);
            }
            catch (Exception e)
            {
                Log.Error(e, "Stack Trace Example {StackTrace}", e.StackTrace);
                return BadRequest(e.Message);
            }
        }

        [HttpPost]
        [Route("Park")]
        public async Task<ActionResult<UserParkingLotResponse>> ParkUser([FromBody] ParkingBLL.ParkRequest request)
        {
            try
            {
                ParkingBLL.UserParkingEvent parkingEvent = await _parkingLotBusinessService.ParkUser(request);
                UserParkingLotResponse response = _mapper.Map<ParkingBLL.UserParkingEvent, UserParkingLotResponse>(parkingEvent);
                return Ok(response);
            }
            catch (Exception e)
            {
                Log.Error(e, "Stack Trace Example {StackTrace}", e.StackTrace);
                return BadRequest(e.Message);
            }
        }

        [HttpPost]
        [Route("Exit")]
        public async Task<ActionResult<UserParkingLotResponse>> DepartUser([FromBody] ParkingBLL.ParkRequest request)
        {
            try
            {
                ParkingBLL.UserParkingEvent parkingEvent = await _parkingLotBusinessService.DepartUser(request);
                UserParkingLotResponse response = _mapper.Map<ParkingBLL.UserParkingEvent, UserParkingLotResponse>(parkingEvent);
                return Ok(response);
            }
            catch (Exception e)
            {
                Log.Error(e, "Stack Trace Example {StackTrace}", e.StackTrace);
                return BadRequest(e.Message);
            }
        }
    }
}