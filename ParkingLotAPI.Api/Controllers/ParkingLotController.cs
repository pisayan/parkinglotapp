﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ParkingLotAPI.API.Models.Response;
using ParkingBLL = ParkingLotAPI.BusinessLayer.Models;
using ParkingLotAPI.BusinessLayer.Services;
using Serilog;

namespace ParkingLotAPI.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class ParkingLotController : ControllerBase
    {
        private readonly IParkingLotBusinessService _parkingLotBusinessService;
        private readonly IMapper _mapper;

        public ParkingLotController(IParkingLotBusinessService lotService, IMapper mapper)
        {
            _parkingLotBusinessService = lotService;
            _mapper = mapper;
        }

        // GET api/Lots
        [HttpGet]
        [Route("Lots")]
        public async Task<ActionResult<IEnumerable<ParkingLotResponse>>> GetLots()
        {
            try
            {
                IEnumerable<ParkingBLL.ParkingLot> lots = await _parkingLotBusinessService.GetParkingLots();
                IEnumerable<ParkingLotResponse> response = _mapper.Map<IEnumerable<ParkingBLL.ParkingLot>, IEnumerable<ParkingLotResponse>>(lots);
                return Ok(response);
            }
            catch (Exception e)
            {
                Log.Error(e, "Stack Trace {StackTrace}", e.StackTrace);
                return BadRequest(e.Message);
            }
        }

        [HttpGet]
        [Route("Lots/{id}")]
        public async Task<ActionResult<ParkingLotResponse>> GetLot(int id)
        {
            try
            {
                ParkingBLL.ParkingLot lot = await _parkingLotBusinessService.GetParkingLot(id);
                return Ok(_mapper.Map<ParkingLotResponse>(lot));
            }
            catch (Exception e)
            {
                Log.Error(e, "Stack Trace Example {StackTrace}", e.StackTrace);
                return BadRequest(e.Message);
            }
        }

        // POST api/Lots
        [HttpPost]
        [Route("Lots")]
        public async Task<ActionResult<ParkingLotResponse>> AddLot([FromBody] ParkingBLL.ParkingLotRequest request)
        {
            try
            {
                ParkingBLL.ParkingLot lot = await _parkingLotBusinessService.AddParkingLot(request);
                return Ok(_mapper.Map<ParkingLotResponse>(lot));
            }
            catch (Exception e)
            {
                Log.Error(e, "Stack Trace Example {StackTrace}", e.StackTrace);
                return BadRequest(e.Message);
            }
        }

        [HttpGet]
        [Route("Lots/CurrentUsers/{id}")]
        public async Task<ActionResult<IEnumerable<UserParkingLotResponse>>> GetCurrentUsers(int id)
        {
            try
            {
                IEnumerable<ParkingBLL.UserParkingEvent> users = await _parkingLotBusinessService.GetLotsCurrentUsers(id);

                return Ok(_mapper.Map<IEnumerable<ParkingBLL.UserParkingEvent>, IEnumerable<UserParkingLotResponse>>(users));
            }
            catch (Exception e)
            {
                Log.Error(e, "Stack Trace Example {StackTrace}", e.StackTrace);
                return BadRequest(e.Message);
            }
        }

        [HttpGet]
        [Route("Lots/UserHistory/{id}")]
        public async Task<ActionResult<IEnumerable<UserParkingLotResponse>>> GetUserHistory(int id)
        {
            try
            {
                IEnumerable<ParkingBLL.UserParkingEvent> users = await _parkingLotBusinessService.GetLotsUserHistory(id);

                return Ok(_mapper.Map<IEnumerable<ParkingBLL.UserParkingEvent>, IEnumerable<UserParkingLotResponse>>(users));
            }
            catch (Exception e)
            {
                Log.Error(e, "Stack Trace Example {StackTrace}", e.StackTrace);
                return BadRequest(e.Message);
            }
        }

        [HttpGet]
        [Route("Lots/FirstAvailableSpot")]
        public async Task<ActionResult<SpotResponse>> GetFirstAvailableSpotInLot(int id, bool handicap)
        {
            try
            {
                ParkingBLL.Spot spot = await _parkingLotBusinessService.GetFirstAvailableSpotInLot(id, handicap);

                return Ok(_mapper.Map<ParkingBLL.Spot, SpotResponse>(spot));
            }
            catch (Exception e)
            {
                Log.Error(e, "Stack Trace Example {StackTrace}", e.StackTrace);
                return BadRequest(e.Message);
            }
        }

        [HttpGet]
        [Route("Lots/GetSpots/{id}")]
        public async Task<ActionResult<IEnumerable<SpotResponse>>> GetSpots(int id)
        {
            try
            {
                IEnumerable<ParkingBLL.Spot> spots = await _parkingLotBusinessService.GetParkingLotsSpots(id);

                return Ok(_mapper.Map<IEnumerable<ParkingBLL.Spot>, IEnumerable<SpotResponse>>(spots));
            }
            catch (Exception e)
            {
                Log.Error(e, "Stack Trace Example {StackTrace}", e.StackTrace);
                return BadRequest(e.Message);
            }
        }
    }
}
