﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ParkingLotAPI.API.Models.Response;
using ParkingBLL = ParkingLotAPI.BusinessLayer.Models;
using ParkingLotAPI.BusinessLayer.Services;
using Serilog;

namespace ParkingLotAPI.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class UserController : Controller
    {
        private readonly IParkingLotBusinessService _parkingLotBusinessService;
        private readonly IMapper _mapper;

        public UserController(IParkingLotBusinessService lotService, IMapper mapper)
        {
            _parkingLotBusinessService = lotService;
            _mapper = mapper;
        }

        // GET api/Users
        [HttpGet]
        [Route("")]
        public async Task<ActionResult<IEnumerable<UserResponse>>> GetUsers()
        {
            try
            {
                IEnumerable<ParkingBLL.User> users = await _parkingLotBusinessService.GetUsers();
                return Ok(_mapper.Map<IEnumerable<ParkingBLL.User>, IEnumerable<UserResponse>>(users));
            }
            catch (Exception e)
            {
                Log.Error(e, "Stack Trace {StackTrace}", e.StackTrace);
                return BadRequest(e.Message);
            }
        }

        // GET api/User/5
        [HttpGet]
        [Route("{id}")]
        public async Task<ActionResult<UserResponse>> GetUser(int id)
        {
            try
            {
                ParkingBLL.User user = await _parkingLotBusinessService.GetUser(id);
                return Ok(_mapper.Map<ParkingBLL.User, UserResponse>(user));
            }
            catch (Exception e)
            {
                Log.Error(e, "Stack Trace Example {StackTrace}", e.StackTrace);
                return BadRequest(e.Message);
            }
        }

        // GET api/User/name@email.com
        [HttpGet]
        [Route("ByEmail")]
        public async Task<ActionResult<UserResponse>> GetUserByEmail(string email)
        {
            try
            {
                ParkingBLL.User user = await _parkingLotBusinessService.GetUser(email);
                return Ok(_mapper.Map<ParkingBLL.User, UserResponse>(user));
            }
            catch (Exception e)
            {
                Log.Error(e, "Stack Trace Example {StackTrace}", e.StackTrace);
                return BadRequest(e.Message);
            }
        }

        // POST api/User
        [HttpPost]
        [Route("add")]
        public async Task<ActionResult> AddUser([FromBody] ParkingBLL.UserRequest request)
        {
            try
            {
                ParkingBLL.User user = await _parkingLotBusinessService.AddUser(request);
                return Ok(user);
            }
            catch (Exception e)
            {
                Log.Error(e, "Stack Trace Example {StackTrace}", e.StackTrace);
                return BadRequest(e.Message);
            }
        }

        [HttpPost]
        [Route("edit")]
        public async Task<ActionResult> EditUser([FromBody] ParkingBLL.UserRequest request)
        {
            try
            {
                ParkingBLL.User user = await _parkingLotBusinessService.EditUser(request);
                return Ok(user);
            }
            catch (Exception e)
            {
                Log.Error(e, "Stack Trace Example {StackTrace}", e.StackTrace);
                return BadRequest(e.Message);
            }
        }

        [HttpGet]
        [Route("GetCurrentParkingLot/{id}")]
        public async Task<ActionResult<ParkingLotResponse>> GetCurrentParkingLot(int id)
        {
            try
            {
                ParkingBLL.ParkingLot lot = await _parkingLotBusinessService.GetUsersCurrentLot(id);
                return Ok(_mapper.Map<ParkingLotResponse>(lot));
            }
            catch (Exception e)
            {
                Log.Error(e, "Stack Trace Example {StackTrace}", e.StackTrace);
                return BadRequest(e.Message);
            }
        }

        [HttpGet]
        [Route("GetCurrentSpot/{id}")]
        public async Task<ActionResult<SpotResponse>> GetCurrentSpot(int id)
        {
            try
            {
                ParkingBLL.Spot spot = await _parkingLotBusinessService.GetUsersCurrentSpot(id);
                return Ok(_mapper.Map<SpotResponse>(spot));
            }
            catch (Exception e)
            {
                Log.Error(e, "Stack Trace Example {StackTrace}", e.StackTrace);
                return BadRequest(e.Message);
            }
        }

        [HttpGet]
        [Route("GetUserHistory/{id}")]
        public async Task<ActionResult<IEnumerable<UserParkingHistoryResponse>>> GetUsersParkingHistory(int id)
        {
            try
            {
               IEnumerable<ParkingBLL.UserParkEventHistoryItem> history = await _parkingLotBusinessService.GetUsersParkingHistory(id);
               return Ok(_mapper.Map<IEnumerable<UserParkingHistoryResponse>>(history));
            }
            catch (Exception e)
            {
                Log.Error(e, "Stack Trace Example {StackTrace}", e.StackTrace);
                return BadRequest(e.Message);
            }
        }
    }
}