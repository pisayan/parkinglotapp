﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.OpenIdConnect;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ParkingLotAPI.Api.Controllers
{
    [AllowAnonymous]
    [Authorize]
    public class AccountController : Controller
    {
        public async Task SignIn()
        {
            await HttpContext.ChallengeAsync(OpenIdConnectDefaults.AuthenticationScheme,
                new AuthenticationProperties() { RedirectUri = "/" });
        }

        public async Task SignOut()
        {
            await HttpContext.SignOutAsync(
              CookieAuthenticationDefaults.AuthenticationScheme);
            await HttpContext.SignOutAsync(
              OpenIdConnectDefaults.AuthenticationScheme);
        }
    }
}