﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ParkingLotBLL = ParkingLotAPI.BusinessLayer.Models;
using ParkingLotAPL = ParkingLotAPI.API.Models;
using AutoMapper;

namespace ParkingLotAPI.API.Maps
{
    public class ParkingLotApiMapProfile : Profile
    {
        public ParkingLotApiMapProfile()
        {
            CreateMap<ParkingLotBLL.User, ParkingLotAPL.Response.UserResponse>();

            CreateMap<ParkingLotBLL.ParkingLot, ParkingLotAPL.Response.ParkingLotResponse>()
                .ForMember(dest => dest.Id, 
                    opts => opts.MapFrom(src => src.ParkingLotId))
                ;

            CreateMap<ParkingLotBLL.UserParkingEvent, ParkingLotAPL.Response.UserParkingLotResponse>()
                .ForMember(dest => dest.UserId,
                    opts => opts.MapFrom(src => src.User.UserId))
                .ForMember(dest => dest.FirstName,
                    opts => opts.MapFrom(src => src.User.FirstName))
                .ForMember(dest => dest.LastName,
                    opts => opts.MapFrom(src => src.User.LastName))
                .ForMember(dest => dest.Email,
                    opts => opts.MapFrom(src => src.User.Email))
                .ForMember(dest => dest.SpotId,
                    opts => opts.MapFrom(src => src.Spot.SpotId))
                ;

            CreateMap<ParkingLotBLL.UserParkEventHistoryItem, ParkingLotAPL.Response.UserParkingHistoryResponse>()
                .ForMember(dest => dest.FirstName,
                    opts => opts.MapFrom(src => src.User.FirstName))
                .ForMember(dest => dest.LastName,
                    opts => opts.MapFrom(src => src.User.LastName))
                .ForMember(dest => dest.Email,
                    opts => opts.MapFrom(src => src.User.Email))
                .ForMember(dest => dest.ParkingLotName,
                    opts => opts.MapFrom(src => src.ParkingLot.Name))
                .ForMember(dest => dest.ParkingLotAddress,
                    opts => opts.MapFrom(src => src.ParkingLot.Address))
                .ForMember(dest => dest.ParkingLotCity,
                    opts => opts.MapFrom(src => src.ParkingLot.City))
                .ForMember(dest => dest.ParkingLotState,
                    opts => opts.MapFrom(src => src.ParkingLot.State))
                .ForMember(dest => dest.ParkingLotZip,
                    opts => opts.MapFrom(src => src.ParkingLot.Zip))
                .ForMember(dest => dest.SpotNumber,
                    opts => opts.MapFrom(src => src.Spot.SpotNumber))
                ;

            CreateMap<ParkingLotBLL.Spot, ParkingLotAPL.Response.SpotResponse>()
                .ForMember(dest => dest.ParkingLotId, 
                    opts => opts.MapFrom(src => src.ParkingLotId))
                .ForMember(dest => dest.SpotId, 
                    opts => opts.MapFrom(src => src.SpotId))
                .ForMember(dest => dest.SpotNumber,
                    opts => opts.MapFrom(src => src.SpotNumber))
                .ForMember(dest => dest.Handicap,
                    opts => opts.MapFrom(src => src.Handicap))
                .ForMember(dest => dest.ParkingRatePerHour,
                    opts => opts.MapFrom(src => src.ParkingRatePerHour))
                .ForMember(dest => dest.SpotId,
                    opts => opts.MapFrom(src => src.SpotId))
                ;
        }
    }
}
